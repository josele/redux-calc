var React = require('react');

var Visor = React.createClass({

  render: function() {
    return (
      <div className="calc-visor">
        {this.props.value}
      </div>
    );
  }

});

module.exports = Visor;