var assign = require('object-assign');

var initialState = {
  value: 0,
  cache: null,
  operation: null,
  result: false,
  clear: false
};

var operations = function (state, action) {

  if (typeof state === 'undefined') {
    return initialState;
  }

  switch (action.type) {
    case 'SET_VALUE':
      return assign({}, state, {
        value: action.value,
        clear: false,
        result: false
      });
    case 'SET_OPERATION':
      return assign({}, state, {
        operation: action.operation,
        clear: true
      });
    case 'SET_CACHE':
      return assign({}, state, {
        cache: action.cache
      });
    case 'RESULT':
      return assign({}, state, {
        value: action.value,
        result: true
      });
    case 'RESET':
      return initialState;
    default:
      return state;
  }
};

function getResult(values, operation) {
  console.log(values, operation)
}


module.exports = operations;