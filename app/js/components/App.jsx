var React = require('react');
var VisibleValue = require('../containers/visibleValue.jsx');
var KeysMap = require('../containers/keysMap.jsx');


var TodoApp = React.createClass({

  render: function() {
    return (
      <div>
        <VisibleValue/>
        <KeysMap />
      </div>
    );
  }

});

module.exports = TodoApp;