var Visor = require('../components/Visor.jsx');
var connect = require('react-redux').connect;

var mapStateToProps = function (state) {
  return {
    value: state.value
  }
};

var VisibleValue = connect(
  mapStateToProps
)(Visor);


module.exports = VisibleValue;