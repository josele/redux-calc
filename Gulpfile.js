
var gulp = require('gulp');
var clean = require('del');
var imagemin = require('gulp-imagemin');
var browserify = require('browserify');
var sass = require('gulp-sass');
var rename = require("gulp-rename");
var source = require('vinyl-source-stream');
var webserver = require('gulp-webserver');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var fs = require('fs');
var reactify = require('reactify');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');


var pathConfig = {
    defaultSrcPath: 'app',
    devPath: '.tmp',
    buildPath: 'www'
};


gulp.task('dev-clean', function () {
    return clean(['.tmp'])
});

gulp.task('dev-browserify', ['dev-clean'], function () {
    return browserify(pathConfig.defaultSrcPath + '/js/app.js', {
        debug: true,
        transform: reactify
    })
        .bundle()
        .on('error', function (err) {
            console.log(err.message);
            this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(pathConfig.devPath + '/js'));
});


gulp.task('dev-optimizeImages', ['dev-clean'], function () {
    return gulp.src(pathConfig.defaultSrcPath + '/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest(pathConfig.devPath + '/images'))
});

gulp.task('dev-copyIndex', ['dev-clean'], function () {
    return gulp.src(pathConfig.defaultSrcPath + '/index.html')
        .pipe(gulp.dest(pathConfig.devPath))
});

//
// gulp.task('dev-copyServices', ['dev-clean'], function () {
//     return gulp.src(pathConfig.defaultSrcPath + '/services/**/*')
//         .pipe(gulp.dest(pathConfig.devPath + '/services'))
// })


gulp.task('dev-sass', ['dev-clean'], function () {
    return gulp.src(pathConfig.defaultSrcPath + '/styles/main.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('styles.css'))
        .pipe(autoprefixer({
            browsers: ['>5%'],
            cascade: true
        }))
        .pipe(gulp.dest(pathConfig.devPath + '/css'))
});

gulp.task('dev-fonts', ['dev-clean'], function () {
    return gulp.src(pathConfig.defaultSrcPath + '/fonts/**/*')
        .pipe(gulp.dest(pathConfig.devPath + '/fonts'))
});

gulp.task('webserver', ['prebuild'], function () {
    gulp.src('.tmp')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            open: true,
            fallback: 'index.html',
            host: '0.0.0.0'
        }));
});

gulp.task('watch', function () {
    gulp.watch([pathConfig.defaultSrcPath + '/**/*'], ['prebuild']);
});

gulp.task('serve', ['prebuild','watch', 'webserver']);

gulp.task('prebuild', ['dev-browserify', 'dev-copyIndex', 'dev-sass', 'dev-optimizeImages', 'dev-fonts']);
