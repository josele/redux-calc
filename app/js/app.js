var ReactDOM = require('react-dom');
var React = require('react');
var Redux = require('redux');
var Provider = require('react-redux').Provider;
var Thunk = require('redux-thunk').default;

var Reducer = require('./reducers/calculator');
var App = require('./components/App.jsx');

var Store = Redux.createStore(Reducer, Redux.applyMiddleware(Thunk));

ReactDOM.render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById('content')
);

// Store.dispatch(Actions.setValue(123));
// Store.dispatch(Actions.setOperation('add'));
// Store.dispatch(Actions.setValue(1231));
