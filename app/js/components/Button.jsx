var React = require('react');

var Button = React.createClass({
  render: function() {
    return (
      <div onClick={this.props.onClick} className="calc-button">
        {this.props.char}
      </div>
    );
  }

});

module.exports = Button;