var Actions = require('../actions/actions');
var Keyboard = require('../components/Keyboard.jsx');
var connect = require('react-redux').connect;


var mapStateToProps = function (state) {
  return {
    value: state.operation
  }
};

var mapDispatchToProps = function (dispatch, ownProps) {
  return {
    onClick: function (number) {
      dispatch(Actions.setValue(number))
    },
    onClickOperation: function (operation) {
      dispatch(Actions.setOperation(operation))
    }
  }
};

var KeyMaps = connect(
  null,
  mapDispatchToProps
)(Keyboard);


module.exports = KeyMaps;

