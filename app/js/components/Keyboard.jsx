var React = require('react');
var Button = require('./Button.jsx');

var Keyboard = React.createClass({

  render: function() {
    var buttons = [];
    var operations = ["+","-","*","/"];

    for(var i = 0; i < 10; i++){
      buttons.push(<Button key={i} onClick={this.props.onClick.bind(this, i)} char={i} />);
    }

    for(var j = 0; j < operations.length; j++){
      buttons.push(<Button key={j+10} onClick={this.props.onClickOperation.bind(this, operations[j])} char={operations[j]} />);
    }

    return (
      <div>
        {buttons}
      </div>
      //React.createElement("div" ,null, React.createElement("h2", null))
    );
  }

});

module.exports = Keyboard;