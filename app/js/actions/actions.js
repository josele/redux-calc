module.exports = {

  setValue: function (value) {
    return function (dispatch, getState) {
      var clear = getState().clear;
      var oldValue = !clear ? getState().value.toString() : 0;
      var val = Number(oldValue + value);
      if (clear) {
        dispatch(setCache(getState().value))
      }
      dispatch(setValue(val));
    }
  },

  setOperation: function (operation) {
    return function (dispatch, getState) {
      if (getState().cache && !getState().result) {
        var result = doOperation(getState());
        dispatch(showResult(result));
      }
      dispatch(setOperation(operation))
    }
  },
  
  reset: function () {
    return {
      type: 'RESET'
    }
  }
};

function setValue(value) {
  return {
    type: 'SET_VALUE',
    value: value
  }
}

function setOperation(operation) {
  return {
    type: 'SET_OPERATION',
    operation: operation
  }
}

function setCache(cache) {
  return {
    type: 'SET_CACHE',
    cache: cache
  }
}

function showResult(result) {
  return {
    type: 'RESULT',
    value: result
  }
}

function doOperation(state) {
  switch (state.operation) {
    case '+':
      return state.cache + state.value;
    case '-':
      return state.cache - state.value;
    case '*':
      return state.cache * state.value;
    case '/':
      return state.cache / state.value;
  }
}