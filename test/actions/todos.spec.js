var expect = require('expect');
var actions = require('../../app/js/actions/actions');
var thunk = require('redux-thunk');
var reduxMock = require('redux-mock-store');
var nock = require('nock');

describe('calc actions', () => {

  it('reset should create RESET action', () => {
    expect(actions.reset()).toEqual({
      type: 'RESET'
    })
  });

  it('set value should create SET_VALUE action', () => {
    const value = 4;
    expect(actions.setValue(value)).toEqual({
      type: 'SET_VALUE',
      value: value
    })
  });


});